# Superatom View

This is a Superatom Framework view library.  
Includes twig example implementation.

## Abstract View class methods

 - public function addData($key, $value);
  - Add shared data.
 - public function getAllData();
  - Return all data.
 - abstract public function render($template, $data = []);
  - Renders a template

## License

MIT
