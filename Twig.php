<?php namespace Superatom\View;

class Twig extends View
{
    /**
     * Extension of template file
     *
     * @var string
     */
    public $fileExtension = '.html.twig';

    /**
     * The options for the Twig environment
     *
     * @var array
     * @see http://twig.sensiolabs.org/doc/api.html#environment-options
     */
    public $twigOptions = [];

    /**
     * Twig instance
     *
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @inheritdoc
     */
    public function render($template, $data = [])
    {
        $this->data = array_merge($this->data, (array) $data);

        return $this->prepareTwig()->render($template . $this->fileExtension, $this->data);
    }

    /**
     * Prepare Twig instance
     *
     * @return \Twig_Environment
     */
    protected function prepareTwig()
    {
        if ($this->twig) {
            return $this->twig;
        }

        $loader = new \Twig_Loader_Filesystem([$this->templatesDir]);
        $this->twig = new \Twig_Environment($loader, $this->twigOptions);

        $this->addTwigExtensions();

        return $this->twig;
    }

    private function addTwigExtensions()
    {
        $ext = $this->twigOptions['extensions'];

        if (in_array('text', $ext)) {
            $this->twig->addExtension(new \Twig_Extensions_Extension_Text());
        }

        if (in_array('i18n', $ext)) {
            $this->twig->addExtension(new \Twig_Extensions_Extension_I18n());
        }

        if (in_array('intl', $ext)) {
            $this->twig->addExtension(new \Twig_Extensions_Extension_Intl());
        }

        if (in_array('date', $ext)) {
            $this->twig->addExtension(new \Twig_Extensions_Extension_Date());
        }

        if (in_array('array', $ext)) {
            $this->twig->addExtension(new \Twig_Extensions_Extension_Array());
        }
    }
}
