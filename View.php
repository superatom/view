<?php namespace Superatom\View;

abstract class View
{
    /**
     * Data available to the all templates
     *
     * @var array
     */
    protected $data = [];

    /**
     * Path to templates base directory
     *
     * @var string
     */
    protected $templatesDir;

    public function __construct($templatesDir)
    {
        $this->templatesDir = $templatesDir;
    }

    public function addData($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function getDataAll()
    {
        return $this->data;
    }

    /**
     * Renders a template
     *
     * @param string $template
     * @param array $data
     * @return string
     */
    abstract public function render($template, $data = []);
}